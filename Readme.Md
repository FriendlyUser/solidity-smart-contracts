[![Build Status](https://travis-ci.org/FriendlyUser/solidity-smart-contracts.svg?branch=master)](https://travis-ci.org/FriendlyUser/solidity-smart-contracts)
# Solidity-smart contracts

Since the solidity programming language is used in multiple blockchain platform including

- Ethereum
- Hedera

learning solidity is invaluable as blockchain technologies and smart contracts will be eseential in the future.


## Documentation

A statically generated site via vuepress and the soldoc solidity documentation which produces markdown.

To generate markdown documentation make sure [soldoc](https://github.com/dev-matan-tsuberi/soldoc) is installed.
```sh
soldoc -o ./docs/contracts --log ./docs/contracts/Soldoc.md
```


### References

* https://github.com/aunyks/vitruvius
* https://github.com/PruthviKumarBK/Decentralized_eCom
* https://github.com/woodydeck/What-Does-Nadia-Think